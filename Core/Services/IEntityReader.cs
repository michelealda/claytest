﻿using System.Collections.Generic;
using Core.Models;

namespace Core.Services
{
    public interface IEntityReader<T> where T : class, IEntity
    {
        T Get(string id);
        IEnumerable<T> GetAll();
    }
}