﻿using Core.Models;

namespace Core.Services
{
    public interface IAuthenticationService
    {
        AuthenticatedUser Authenticate(UnauthenticatedUser unauthenticatedUser);
    }
}