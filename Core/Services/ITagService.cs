﻿using Core.Models;

namespace Core.Services
{
    public interface ITagService : IEntityReader<Tag>
    {
        Tag AuthorizeTagToOpen(string tagId, string doorId);
    }
}