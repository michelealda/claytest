﻿using System;
using System.Collections.Generic;
using Core.Models;

namespace Core.Services
{
    public interface IDoorEventService
    {
        void StoreEvent(DoorEvent doorEvent);

        IEnumerable<DoorEvent> GetEventsByDoor(string doorId,
            DateTime from, 
            DateTime to);

        IEnumerable<DoorEvent> GetEventsByTag(string tagId, 
            DateTime from,
            DateTime to);
    }
}