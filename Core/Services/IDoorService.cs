﻿using Core.Models;

namespace Core.Services
{
    public interface IDoorService : IEntityReader<Door>
    {
        Door OpenTheDoor(string tagId, string doorId);
        Door EnsureDoorIsClosed(string doorId);
    }
}