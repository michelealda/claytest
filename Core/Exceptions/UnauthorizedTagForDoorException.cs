﻿using System;

namespace Core.Exceptions
{
    public class UnauthorizedTagForDoorException : Exception
    {
        public UnauthorizedTagForDoorException(string message) : base(message)
        {
            
        }
    }
}