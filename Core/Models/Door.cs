﻿using System;

namespace Core.Models
{
    public class Door : IEntity
    {
        private Door()
        {
        }

        public Door(string id,
            DoorStatus status,
            DateTime lastUpdate)
        {
            Id = id;
            Status = status;
            LastUpdate = lastUpdate;
        }

        public string Id { get; }
        public DoorStatus Status { get; }
        public DateTime LastUpdate { get; }

        public Door Unlock()
            => TransitionDoorStatus(DoorStatus.Unlocked);

        public Door Lock()
            => TransitionDoorStatus(DoorStatus.Locked);

        private Door TransitionDoorStatus(DoorStatus newStatus)
            => Status == newStatus
                ? this
                : new Door(Id, newStatus, DateTime.UtcNow);
    }

    public enum DoorStatus
    {
        Locked = 0,
        Unlocked
    }
}