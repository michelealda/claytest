﻿namespace Core.Models
{
    public class UnauthenticatedUser
    {
        public UnauthenticatedUser(
            string email,
            string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; }
        public string Password { get; }
    }

    public class AuthenticatedUser 
    {
        public AuthenticatedUser(
            string id,
            string email,
            string tag)
        {
            Id = id;
            Email = email;
            Tag = tag;
        }
        
        public string Id { get; }
        public string Email { get; }
        public string Tag { get; }
    }
}