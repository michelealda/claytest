﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper.Contrib.Extensions;

namespace Core.Models
{
    public class Tag : IEntity
    {
        private Tag()
        {
        }
        private readonly IEnumerable<Door> _doors;
        public Tag(
            string id,
            TagStatus status,
            IEnumerable<Door> doors)
        {
            Id = id;
            Status = status;
            _doors = doors;
        }

        public string Id { get; }
        public TagStatus Status { get; }

        [Computed]
        public IEnumerable<Door> Doors => _doors ?? Enumerable.Empty<Door>();
        
        public bool HasAccessTo(Door door)
            => Doors.Any(d => d.Id == door.Id);

        public Tag WithAccessTo(Door door)
        {
            if (Status == TagStatus.Inactive)
                throw new InvalidOperationException("Inactive Tag");
            
            return new Tag(Id,
                Status,
                HasAccessTo(door)
                    ? Doors
                    : Doors.Concat(new[] { door }));
        }

        public bool CanOpen(Door door)
            => Status == TagStatus.Active && HasAccessTo(door) ;

        public Tag Deactivate()
            => new Tag(Id,
                TagStatus.Inactive,
                Doors
                );
    }

    public enum TagStatus
    {
        Inactive = 0,
        Active
    }
}