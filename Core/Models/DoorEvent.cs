﻿using System;
using Core.Exceptions;
using Dapper.Contrib.Extensions;

namespace Core.Models
{
    public enum EventType
    {
        DoorOpened,
        DoorClosed
    }

    public class DoorEvent
    {
        private DoorEvent()
        {
        }

        public DoorEvent(DateTime eventTime, 
            string doorId, 
            EventType type, 
            string tagId)
        {
            EventTime = eventTime;
            DoorId = doorId;
            Type = type;

            if (Type == EventType.DoorOpened 
                && string.IsNullOrEmpty(tagId))
                throw new InvalidArgumentException($"{nameof(TagId)} is required");
            
            TagId = tagId;
        }

        [Key]
        public DateTime EventTime { get; }
        public string DoorId { get; }
        public EventType Type { get; }
        public string TagId { get; }
    }
}