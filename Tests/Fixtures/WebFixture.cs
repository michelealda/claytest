﻿using System;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Infrastructure.Database;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace Tests.Fixtures
{
    public class WebFixture : IDisposable
    {
        private readonly IServiceProvider _services;

        protected readonly HttpClient Client;
        protected readonly IDbConnection Connection;

        public WebFixture()
        {
            var builder = WebHost.CreateDefaultBuilder()
                .UseStartup<TestStartup>();
            
            var server = new TestServer(builder);
            Client = server.CreateClient();
            //by default all my api will required an authenticated user
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, ValidToken);
            _services = server.Host.Services;
            Connection = GetService<IDbConnection>();
            Connection.CreateDb();
            Connection.SeedDatabase();
        }

        protected T GetService<T>() => (T)_services.GetService(typeof(T));

        protected const string ValidToken =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInNpZCI6IkFBQUFBIiwiZXhwIjo0MTAyNDQ0ODAwfQ.75mgM2Mcz79OQ5KFtPHiG56gJGQYb-3SJCZlyTz4Jzk";

        protected const string ValidTag = "AAAAA";
        protected const string TunnelDoor = "tunnel";

        public void Dispose()
        {
            Connection?.Close();
        }
    }
}