﻿using System.Data;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using webapi;

namespace Tests.Fixtures
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public override void ConfigureDatabase(IServiceCollection services)
        {
            services.AddSingleton<IDbConnection>(_ =>
            {
                var connection =
                    new SqliteConnection("DataSource=:memory:;cache=shared;");
                connection.Open();
                return connection;
            });
        }
    }
}