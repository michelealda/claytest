using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core.Models;
using Dapper;
using FluentAssertions;
using Newtonsoft.Json;
using Tests.Factories;
using Tests.Fixtures;
using Xunit;

namespace Tests.IntegrationTests
{
    public class EventsControllerTests : WebFixture
    {
        [Fact]
        public async Task ShouldReturnEventsInReverseChronologicalOrder()
        {
            DoorEventFactory.GeneratePastEventsFor(ValidTag, TunnelDoor, 5)
                .ToList()
                .ForEach(e =>
                    Connection.Execute(
                        "INSERT INTO DoorEvents (EventTime, DoorId, Type, TagId) VALUES (@eventTime, @doorId, @type, @tagId)",
                        new
                        {
                            eventTime = e.EventTime,
                            doorId = e.DoorId,
                            type = e.Type,
                            tagId = e.TagId
                        }));

            var response = await Client.GetAsync($"api/events/door/{TunnelDoor}");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<DoorEvent>>(responseString).ToArray();

            result.Should().HaveCount(5);
            result.Should().BeInDescendingOrder(e => e.EventTime);
        }
        [Fact]
        public async Task ShouldFilterEventsInThePast()
        {
            var now = DateTime.Now;
            var fiveMinsAgo = now.AddMinutes(-5);
            DoorEventFactory.GeneratePastEventsFor(ValidTag, TunnelDoor, 5)
                // Add an event for today
                .Concat(new[] { new DoorEvent(now, TunnelDoor, EventType.DoorOpened, ValidTag) })
                .ToList()
                .ForEach(e =>
                    Connection.Execute(
                        "INSERT INTO DoorEvents (EventTime, DoorId, Type, TagId) VALUES (@eventTime, @doorId, @type, @tagId)",
                        new
                        {
                            eventTime = e.EventTime,
                            doorId = e.DoorId,
                            type = e.Type,
                            tagId = e.TagId
                        }));
            
            var response = await Client.GetAsync(
                $"api/events/door/{TunnelDoor}?from={HttpUtility.UrlEncode(fiveMinsAgo.ToString("O"))}");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<DoorEvent>>(responseString).ToArray();

            result.Should().HaveCount(1);
            result.First()
                .DoorId
                .Should().Be(TunnelDoor);
        }
    }
}