using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Core.Models;
using FluentAssertions;
using Newtonsoft.Json;
using Tests.Fixtures;
using Xunit;

namespace Tests.IntegrationTests
{
    public class TagsControllerTests : WebFixture
    {
        [Fact]
        public async Task ShouldReturnTags()
        {
            var response = await Client.GetAsync("api/tags");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<Tag>>(responseString);

            result
                .First()
                .Id
                .Should().Be(ValidTag);
        }

        [Fact]
        public async Task ShouldReturnTagById()
        {
            var response = await Client.GetAsync($"api/tags/{ValidTag}");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Tag>(responseString);

            result
                .Id
                .Should().Be(ValidTag);
        }

        [Fact]
        public async Task AuthorizeTagOnDoorIsIdempotent()
        {
            var door = new Door(TunnelDoor, DoorStatus.Locked, new DateTime(2018, 1, 1, 0, 0, 0));
            var response = await Client.PostAsync($"api/tags/{ValidTag}/authorize/{door.Id}", null);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Tag>(responseString);

            result
                .Doors
                .Should().ContainSingle(d => d.Id == door.Id);
        }
        
        [Fact]
        public async Task ShouldReturnNotFoundIfTagDoesntExist()
        {
            var response = await Client.GetAsync("api/tags/ZZZZZ");
            response.StatusCode
                .Should().Be(HttpStatusCode.NotFound);
        }
    }
}