using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Dapper;
using Dapper.Contrib.Extensions;
using FluentAssertions;
using Newtonsoft.Json;
using Tests.Fixtures;
using Xunit;

namespace Tests.IntegrationTests
{
    public class DoorControllerTests : WebFixture
    {
        [Fact]
        public async Task ShouldReturnDoors()
        {
            var response = await Client.GetAsync("api/doors");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<Door>>(responseString);

            result.Should().HaveCount(2);
        }

        [Fact]
        public async Task DoorShouldBeUnlockedByTag()
        {
            var response = await Client.PostAsync($"api/doors/{TunnelDoor}/unlock",
                new StringContent(JsonConvert.SerializeObject(ValidTag), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Door>(responseString);

            result
                .Status
                .Should().Be(DoorStatus.Unlocked);
            result
                .LastUpdate
                .Should().BeCloseTo(DateTime.UtcNow, 100);

            var dbValue = Connection.Get<Door>(TunnelDoor);

            dbValue
                .Status
                .Should().Be(DoorStatus.Unlocked);
            dbValue
                .LastUpdate
                .Should().BeCloseTo(DateTime.UtcNow, 100);
        }

        [Fact]
        public async Task DoorUnlockedShouldBePresentAsEvent()
        {
            var response = await Client.PostAsync($"api/doors/{TunnelDoor}/unlock",
                new StringContent(JsonConvert.SerializeObject(ValidTag), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();

            var events = Connection.Query<DoorEvent>("SELECT * FROM DoorEvents").ToArray();
            events
                .Should().HaveCount(1);
            events
                .First()
                .EventTime
                .Should().BeCloseTo(DateTime.UtcNow, 100);
        }
    }
}