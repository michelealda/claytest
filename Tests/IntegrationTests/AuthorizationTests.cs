using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using FluentAssertions;
using Newtonsoft.Json;
using Tests.Fixtures;
using Xunit;

namespace Tests.IntegrationTests
{
    public class AuthorizationTests : WebFixture
    {
        [Fact]
        public async Task AnonimousUserShouldGetUnauthorizeResponse()
        {
            // I reset the Authorization token as it's by default set by 
            // the WebFixture
            Client.DefaultRequestHeaders.Authorization = null;
            var response = await Client.GetAsync("api/tags");
            response.StatusCode
                .Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task AuthenticatedUserShouldOkResponse()
        {
            var response = await Client.GetAsync("api/tags");
            response.StatusCode
                .Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ValidUserWillGetToken()
        {
            var user = new UnauthenticatedUser("test@test.com", "test");
            Client.DefaultRequestHeaders.Authorization = null;
            var response = await Client.PostAsync("api/token",
                new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json"));

            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<dynamic>(responseString);
            Assert.Equal(ValidToken, (string)obj.token);
        }
    }
}