using System;
using FluentAssertions;
using Tests.Factories;
using Xunit;

namespace Tests.UnitTests
{
    public class TagTests
    {
        [Fact]
        public void TagCanOpenAuthorizedDoor()
        {
            var door = DoorFactory.CreateLocked();
            var tag = TagFactory
                .Create()
                .WithAuthorizedDoors(2)
                .WithAccessTo(door);

            tag.CanOpen(door)
                .Should()
                .BeTrue();
        }

        [Fact]
        public void CannotAssignDoorToInactiveTag()
        {
            var door = DoorFactory.CreateLocked();
            var tag = TagFactory
                .Create()
                .Deactivate();

            tag.Invoking(t => t.WithAccessTo(door))
                .Should()
                .Throw<InvalidOperationException>()
                .WithMessage("Inactive Tag");
        }
    }
}