using System;
using Core;
using Core.Exceptions;
using Core.Models;
using FluentAssertions;
using Xunit;

namespace Tests.UnitTests
{
    public class EventTests
    {
        [Fact]
        public void DoorOpenEventRequiresATagArgument()
        {
            Action action = () => new DoorEvent(
                DateTime.UtcNow,
                "doorId",
                EventType.DoorOpened,
                null
            );

            action
                .Should()
                .Throw<InvalidArgumentException>()
                .WithMessage("TagId is required");
        }
    }
}