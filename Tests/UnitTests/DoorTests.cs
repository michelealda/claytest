using System;
using Core.Models;
using FluentAssertions;
using Tests.Factories;
using Xunit;

namespace Tests.UnitTests
{
    public class DoorTests
    {
        [Fact]
        public void UnlockAClosedDoor()
            => DoorFactory.CreateLocked()
                .Unlock()
                .Status
                .Should()
                .Be(DoorStatus.Unlocked);

        [Fact]
        public void UnlockAClosedDoorUpdateTheLastUpdateValue()
            => DoorFactory.CreateLocked()
                .Unlock()
                .LastUpdate
                .Should()
                .BeCloseTo(DateTime.UtcNow, 100);

        [Fact]
        public void LockALockedDoorWontChangeState()
        {
            var door = DoorFactory.CreateLocked();
            var lockedDoor = door.Lock();

            lockedDoor
                .Status
                .Should().Be(door.Status);

            lockedDoor
                .LastUpdate
                .Should().Be(door.LastUpdate);
        } 
    }
}
