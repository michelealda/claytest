using System;
using System.Collections.Generic;
using Bogus;
using Core.Models;

namespace Tests.Factories
{
    public static class DoorEventFactory
    {
        public static IEnumerable<DoorEvent> GeneratePastEventsFor(string tagId,
            string doorId, int count = 1)
            => new Faker<DoorEvent>()
                .CustomInstantiator(f => new DoorEvent(
                    f.Date.Recent(10, DateTime.UtcNow).AddDays(-1),
                    doorId,
                    EventType.DoorOpened,
                    tagId
                ))
                .Generate(count);
    }
}