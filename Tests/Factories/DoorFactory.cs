using System.Collections.Generic;
using System.Linq;
using Bogus;
using Core.Models;

namespace Tests.Factories
{
    public static class DoorFactory
    {
        public static Door CreateLocked()
            => new Faker<Door>()
                .CustomInstantiator(f => new Door(
                    f.Address.StreetAddress(),
                    DoorStatus.Locked,
                    f.Date.Recent()
                ))
                .Generate();

        public static IEnumerable<Door> Create(int count = 1)
            => Enumerable.Range(1, count).Select(_ => CreateLocked());
    }
}