using System.Linq;
using Bogus;
using Core.Models;

namespace Tests.Factories
{
    public static class TagFactory
    {
        public static Tag Create()
            => new Faker<Tag>()
                .CustomInstantiator(f => new Tag(
                    f.Random.AlphaNumeric(10),
                    TagStatus.Active,
                    Enumerable.Empty<Door>()
                ))
                .Generate();

        public static Tag WithAuthorizedDoors(this Tag tag, int count = 1)
            => DoorFactory.Create(count)
                .Aggregate(tag, 
                    (t, d) => t.WithAccessTo(d));
    }
}