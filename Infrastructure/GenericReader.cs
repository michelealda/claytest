﻿using System.Collections.Generic;
using System.Data;
using Core.Exceptions;
using Core.Models;
using Core.Services;
using Dapper.Contrib.Extensions;

namespace Infrastructure
{
    public abstract class GenericReader<T> : IEntityReader<T> where T : class, IEntity
    {
        private readonly IDbConnection _connection;

        protected GenericReader(IDbConnection connection)
        {
            _connection = connection;
        }

        public virtual T Get(string id)
        {
            var x = _connection.Get<T>(id);
            if (x == null)
                throw new RecordNotFoundException(
                    $"Cannot find {typeof(T).Name} with id = {id}");
            return x;
        }

        IEnumerable<T> IEntityReader<T>.GetAll()
            => _connection.GetAll<T>();
            
    }
}
