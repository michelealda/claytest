﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Exceptions;
using Core.Models;
using Core.Services;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Infrastructure.Services
{
    public class TagService : GenericReader<Tag>, ITagService
    {
        private readonly IDbConnection _connection;

        public TagService(IDbConnection connection)
            : base(connection)
        {
            _connection = connection;
        }

        public override Tag Get(string id)
        {
            var tag = base.Get(id);
            var authorizedDoors = GetDoorsForTag(tag.Id);
            return authorizedDoors
                .Aggregate(tag, (t, d) => t.WithAccessTo(d));
        }

        public Tag AuthorizeTagToOpen(string tagId, string doorId)
        {
            var tag = Get(tagId);

            var door = _connection.Get<Door>(doorId);
            if (door == null)
                throw new RecordNotFoundException($"Cannot find {typeof(Door).Name} with id = {doorId}");

            var newTag = tag.WithAccessTo(door);

            if (newTag.HasAccessTo(door))
                _connection.Execute(
                    "INSERT INTO TagDoors VALUES (@tagId, @doorId)",
                    new { tagId = newTag.Id, doorId = door.Id });

            return newTag;
        }

        private IEnumerable<Door> GetDoorsForTag(string tagId)
            => _connection.Query<Door>(
                @"SELECT d.* 
                  FROM Doors d 
                  INNER JOIN TagDoors td ON d.Id = td.DoorId 
                  WHERE td.TagId = @tagId",
                new { tagId });
    }
}