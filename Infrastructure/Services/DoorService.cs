﻿using System;
using System.Data;
using Core.Models;
using Core.Services;
using Dapper;

namespace Infrastructure.Services
{
    public class DoorService : GenericReader<Door>, IDoorService
    {
        private readonly IDbConnection _connection;
        private readonly ITagService _tagService;
        private readonly IDoorEventService _doorEventService;

        public DoorService(IDbConnection connection,
            ITagService tagService,
            IDoorEventService doorEventService
            )
            : base(connection)
        {
            _connection = connection;
            _tagService = tagService;
            _doorEventService = doorEventService;
        }

        public Door OpenTheDoor(string tagId, string doorId)
        {
            var door = Get(doorId);
            var tag = _tagService.Get(tagId);

            if (!tag.CanOpen(door))
                throw new UnauthorizedAccessException(
                    $"Tag {tag.Id} not authorized on door {door.Id}");

            var unlockeDoor = door.Unlock();
            UpdateDoor(door.Unlock());
            _doorEventService.StoreEvent(new DoorEvent(DateTime.UtcNow,
                    door.Id,
                    EventType.DoorOpened,
                    tag.Id
                )
            );
            return unlockeDoor;
        }

        public Door EnsureDoorIsClosed(string doorId)
        {
            var lockedDoor = Get(doorId).Lock();
            UpdateDoor(lockedDoor);
            return lockedDoor;
        }

        private void UpdateDoor(Door door)
            => _connection.Execute("UPDATE Doors " +
                                   "SET Status = @status, LastUpdate = @lastUpdate " +
                                   "WHERE Id = @id",
                new
                {
                    id = door.Id,
                    status = door.Status,
                    lastUpdate = door.LastUpdate
                });
    }
}