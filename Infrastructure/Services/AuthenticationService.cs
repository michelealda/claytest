﻿using System.Data;
using Core.Models;
using Core.Services;
using Dapper;

namespace Infrastructure.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IDbConnection _connection;

        public AuthenticationService(IDbConnection connection)
        {
            _connection = connection;
        }

        public AuthenticatedUser Authenticate(UnauthenticatedUser unauthenticatedUser)
        {
            //for simplicity I omit the password check
            return _connection.QuerySingle<AuthenticatedUser>(
                 "SELECT Id, Email, Tag FROM Users WHERE Email = @email",
                 new { email = unauthenticatedUser.Email });
        }
    }
}