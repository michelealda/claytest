﻿using System;
using System.Collections.Generic;
using System.Data;
using Core.Models;
using Core.Services;
using Dapper;

namespace Infrastructure.Services
{
    public class DoorEventService : IDoorEventService
    {
        private readonly IDbConnection _connection;

        public DoorEventService(IDbConnection connection)
        {
            _connection = connection;
        }

        public void StoreEvent(DoorEvent doorEvent)
            => _connection.Execute(@"INSERT INTO DoorEvents 
                        (EventTime, DoorId, Type, TagId) 
                        VALUES (@EventTime, @DoorId, @Type, @TagId)",
                new
                {
                    doorEvent.EventTime,
                    doorEvent.DoorId,
                    doorEvent.Type,
                    doorEvent.TagId
                });


        public IEnumerable<DoorEvent> GetEventsByDoor(string doorId,
            DateTime from,
            DateTime to)
            => GetEventsInRange(filterDoor: true,
                id: doorId,
                fromTime: from,
                toTime: to);

        public IEnumerable<DoorEvent> GetEventsByTag(string tagId,
            DateTime from,
            DateTime to)
            => GetEventsInRange(filterDoor: false,
                id: tagId,
                fromTime: from,
                toTime: to);

        private IEnumerable<DoorEvent> GetEventsInRange(bool filterDoor,
            string id,
            DateTime fromTime,
            DateTime toTime)
        {
            var selector = filterDoor
                ? "DoorId"
                : "TagId";
            return _connection.Query<DoorEvent>(
                $@"SELECT EventTime, DoorId, Type, TagId
                        FROM DoorEvents 
                       WHERE {selector} = @id
                       AND EventTime >= @fromTime
                       AND EventTime <= @toTime
                       ORDER BY EventTime DESC",
                new
                {
                    id,
                    fromTime,
                    toTime
                });
        }
    }
}