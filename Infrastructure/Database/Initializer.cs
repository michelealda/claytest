﻿using System;
using System.Data;
using Core.Models;
using Dapper;

namespace Infrastructure.Database
{
    public static class Initializer
    {
        public static void CreateDb(this IDbConnection connection)
        {
            connection.Execute(
                @"create table Tags
            (
                Id              text primary key,
                Status          text not null
            )");
            connection.Execute(
                @"create table Doors
            (
                Id              text primary key,
                Status          text not null,
                LastUpdate      timestamp not null
            )");
            connection.Execute(
                @"create table TagDoors
            (
                TagId             text not null,
                DoorId            text not null,
                FOREIGN KEY(TagId) REFERENCES Tags(Id),
                FOREIGN KEY(DoorId) REFERENCES Doors(Id)
            )");
            connection.Execute(
                @"create table DoorEvents
            (
                EventTime       timestamp not null,
                DoorId          text not null,
                Type            text not null,
                TagId           text null,
                FOREIGN KEY(TagId) REFERENCES Tags(Id),
                FOREIGN KEY(DoorId) REFERENCES Doors(Id)
            )");
            connection.Execute(
                @"create table Users
            (
                Id              text primary key,
                Email           text not null,
                Password        text not null,
                Tag             text null,
                FOREIGN KEY(Tag) REFERENCES Tags(Id)
            )");
        }

        public static void SeedDatabase(this IDbConnection connection)
        {
            const string tagId = "AAAAA";
            connection.Execute(
                            "INSERT INTO Tags VALUES (@id, @status)",
                            new { id = tagId, status = TagStatus.Active });

            connection.Execute(
                "INSERT INTO Users VALUES (@id, @email, @password, @tag)",
                new { id = "admin", email = "test@test.com", password = "test", tag = tagId });

            connection.Execute(
                "INSERT INTO Doors VALUES (@id, @status, @lastUpdate)",
                new { id = "tunnel", status = DoorStatus.Locked, lastUpdate = new DateTime(2018, 1, 1, 0, 0, 0) });

            connection.Execute(
                "INSERT INTO Doors VALUES (@id, @status, @lastUpdate)",
                new { id = "frontDoor", status = DoorStatus.Locked, lastUpdate = new DateTime(2018, 1, 1, 0, 0, 0) });

            connection.Execute(
                "INSERT INTO TagDoors VALUES (@tagId, @doorId)",
                new { tagId, doorId = "tunnel" });

            connection.Execute(
                "INSERT INTO TagDoors VALUES (@tagId, @doorId)",
                new { tagId, doorId = "frontDoor" });


        }
    }
}
