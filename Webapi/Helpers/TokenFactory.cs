﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Core.Models;
using Microsoft.IdentityModel.Tokens;

namespace webapi
{
    public static class TokenFactory
    {
        public const string SecretKey = "secretsecretsecret";
        public static string BuildToken(AuthenticatedUser user, string secretKey)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                // for simplicity I will no implement a custom claim
                // but I will use the standard sid to store the assigned Tag to the user
                new Claim(JwtRegisteredClaimNames.Sid, user.Tag), 
            };

            var signingKey =
                new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(secretKey));
            var creds = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: new DateTime(2100,1,1,0,0,0),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}