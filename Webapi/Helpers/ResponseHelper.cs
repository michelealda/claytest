﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Webapi.Helpers
{
    public static class ResponseHelper
    {
        public static Task InternalServerErrorResponse(HttpContext httpContext)
            => BuildResponse(httpContext, HttpStatusCode.InternalServerError,
                new { Message = "An error occured" });

        public static Task BadRequestResponse(HttpContext httpContext, Exception ex)
            => BuildResponse(httpContext, HttpStatusCode.BadRequest,
                new { ex.Message });

        public static Task NotFoundResponse(HttpContext httpContext,
            Exception ex)
            => BuildResponse(httpContext, HttpStatusCode.NotFound,
                new { ex.Message });

        public static Task UnauthorizedResponse(HttpContext httpContext,
            Exception ex)
            => BuildResponse(httpContext, HttpStatusCode.Unauthorized,
                new { ex.Message });

        private static async Task BuildResponse(
            HttpContext httpContext,
            HttpStatusCode code, object payload)
        {
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = (int)code;
            httpContext.Response.ContentType = "application/json";

            await httpContext.Response.WriteAsync(
                JsonConvert.SerializeObject(payload));
        }
    }
}