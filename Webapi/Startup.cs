﻿using System.Data;
using System.IO;
using System.Text;
using Core.Services;
using Infrastructure.Database;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Webapi.Filters;
using Webapi.Middlewares;

namespace webapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureDatabase(services);

            services.AddSingleton<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IDoorService, DoorService>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IDoorEventService, DoorEventService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var signingKey =
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(TokenFactory.SecretKey));
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        RequireExpirationTime = false,
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        IssuerSigningKey = signingKey
                    };
                });

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelAttribute));
            });
        }

        public virtual void ConfigureDatabase(IServiceCollection services)
        {
            services.AddTransient<IDbConnection>(_ => new SqliteConnection("DataSource=MyData.db"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (!File.Exists(DatabaseName))
            {
                using (var conn =
                    new SqliteConnection($"DataSource={DatabaseName}"))
                {
                    conn.CreateDb();
                    conn.SeedDatabase();
                }
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // this middleware will let the exception happen on the pipeline but 
            // will map it to the appropriate response message
            app.UseExceptionMiddleware();
            app.UseAuthentication();
            app.UseMvc();
        }

        private const string DatabaseName = "data.db";
    }
}
