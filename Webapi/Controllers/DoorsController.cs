﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Core.Models;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    [Route("api/doors"), Authorize]
    public class DoorsController : Controller
    {
        private readonly IDoorService _doorService;

        public DoorsController(IDoorService doorService)
        {
            _doorService = doorService;
        }

        [HttpGet]
        public IEnumerable<Door> Get()
            => _doorService.GetAll();

        [HttpGet("{id}")]
        public Door Get(string id)
            => _doorService.Get(id);

        [HttpPost("{id}/unlock")]
        public Door Unlock(string id)
        {
            var tagId = (HttpContext.User.Identity as ClaimsIdentity)?
                .FindFirst(JwtRegisteredClaimNames.Sid).Value;
            return _doorService.OpenTheDoor(tagId, id);
        }

        [HttpPost("{id}/lock")]
        public Door Lock(string id)
            => _doorService.EnsureDoorIsClosed(id);
    }
}