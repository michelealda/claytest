﻿using System.Collections.Generic;
using Core.Models;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    [Route("api/tags"), Authorize]
    public class TagsController : Controller
    {
        private readonly ITagService _tagService;

        public TagsController(ITagService tagService)
        {
            _tagService = tagService;
        }
        
        [HttpGet]
        public IEnumerable<Tag> Get()
         => _tagService.GetAll();
        
        // an record not found exception might rise from the service
        // it will caught by a middleware and the proper response message
        // will be returned to the consumer
        [HttpGet("{id}")]
        public Tag Get(string id)
            => _tagService.Get(id);

        [HttpPost("{id}/authorize/{doorId}")]
        public Tag AuthorizeDoor(string id, string doorId)
            => _tagService.AuthorizeTagToOpen(id, doorId);
    }
}
