﻿using Core.Models;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace webapi.Controllers
{
    [Route("api/token")]
    public class TokenController : Controller
    {
        private readonly IAuthenticationService _authenticationService;

        public TokenController(IConfiguration config,
            IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        
        [HttpPost, AllowAnonymous]
        public IActionResult CreateToken([FromBody] UnauthenticatedUser model)
        {
            var user = _authenticationService.Authenticate(model);

            if (user == null) return Unauthorized();

            var tokenString = TokenFactory.BuildToken(
                user,
                TokenFactory.SecretKey);
            return Ok(new { token = tokenString });
        }
    }
}
