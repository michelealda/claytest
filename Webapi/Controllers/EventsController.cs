﻿using System;
using System.Collections.Generic;
using Core;
using Core.Exceptions;
using Core.Models;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Webapi.Controllers
{
    [Route("api/events"), Authorize]
    public class EventsController : Controller
    {
        private readonly IDoorEventService _doorEventService;

        public EventsController(IDoorEventService doorEventService)
        {
            _doorEventService = doorEventService;
        }

        [HttpGet("door/{id}")]
        public IEnumerable<DoorEvent> GetEventsByDoor(string id,
            DateTime from,
            DateTime to)
            => GetEvents(id, from, to, _doorEventService.GetEventsByDoor);

        [HttpGet("tag/{id}")]
        public IEnumerable<DoorEvent> GetEventsByTag(string id,
            DateTime from,
            DateTime to)
            => GetEvents(id, from, to, _doorEventService.GetEventsByTag);

        private static IEnumerable<DoorEvent> GetEvents(
            string id,
            DateTime from,
            DateTime to,
            Func<string, DateTime, DateTime, IEnumerable<DoorEvent>> fetchFn)
        {
            to = to == DateTime.MinValue 
                ? DateTime.MaxValue 
                : to;
            if (to < from)
                throw new InvalidArgumentException("Inconsistent date range");
            return fetchFn(id, from, to);
        }
    }
}