﻿using System;
using System.Threading.Tasks;
using Core.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Webapi.Helpers;

namespace Webapi.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next,
            ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory
                .CreateLogger<ExceptionMiddleware>();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                switch (ex)
                {
                    case RecordNotFoundException _:
                        await ResponseHelper.NotFoundResponse(httpContext, ex);
                        _logger.LogWarning($"{ex.Message}");
                        break;
                    case UnauthorizedTagForDoorException _:
                        await ResponseHelper.UnauthorizedResponse(httpContext, ex);
                        _logger.LogWarning($"{ex.Message}");
                        break;
                    case InvalidArgumentException _:
                        await ResponseHelper.BadRequestResponse(httpContext, ex);
                        _logger.LogWarning($"{ex.Message}");
                        break;
                    default:
                        //If is not a known exception return it but do not expose reason as it could be a security threat
                        await ResponseHelper.InternalServerErrorResponse(httpContext);
                        _logger.LogError($"{ex.Message}");
                        break;
                }
            }
        }
    }

    public static class ExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
